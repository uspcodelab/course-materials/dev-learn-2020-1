function add() {
  const title = document.getElementById('todoTitle').value;
  const description = document.getElementById('todoDescription').value;

  const newPostIt = document.createElement('div');
  const postItTitle = document.createElement('h2');
  const postItDescription = document.createElement('p');

  postItTitle.textContent = title;
  postItDescription.textContent = description;

  newPostIt.className = `post-it ${getRandomPostItColor()}`;
  newPostIt.appendChild(postItTitle);
  newPostIt.appendChild(postItDescription);
  newPostIt.onclick = () => remove();

  document.getElementById('board').appendChild(newPostIt);
}

function getRandomPostItColor() {
  const random = Math.floor(Math.random() * 4);
  switch(random) {
    case 0:
      return 'blue';
      break;
    case 1:
      return 'orange';
      break;
    case 2:
      return 'pink';
      break;
    case 3:
      return 'yellow';
      break;
    default:
      return 'blue';
      break;
  } 
}

function remove($event) {
  const target = event.target || event.srcElement;
  target.remove();
}